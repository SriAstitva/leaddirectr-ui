var ngApp = angular.module('myApp', ['ngRoute','ui.bootstrap', 'ngAnimate', 'ngCookies']);


//router
ngApp.config(function ($routeProvider) {
    $routeProvider

        .when('/main/',
        {
            controller: 'MainController',
            templateUrl: 'ng-templates/main.html',
              resolve: {
                tredingSubreddits: function ($http, AJAXService) {
                    //this returns promise
                  return AJAXService.getTredingSubReddits();
                }
              }

        })
		
		.when('/',
        {
            controller: 'HomeController',
            templateUrl: 'ng-templates/home.html'

        })
		.when('/quizstart/',
        {
            controller: 'QuizstartController',
            templateUrl: 'ng-templates/quizstart.html'

        })

        .when('/path1/',
        {
            controller: 'Path1Controller',
            templateUrl: 'ng-templates/path1.html',
              resolve: {
                tredingSubreddits: function ($http, AJAXService) {
                    //this returns promise
                  return AJAXService.getTredingSubReddits();
                }
              }

        })

        .when('/path2/',
        {
            controller: 'Path2Controller',
            templateUrl: 'ng-templates/path2.html'

        })

        .when('/faqs/',
        {
            controller: 'FaqsController',
            templateUrl: 'ng-templates/faqs.html'

        })

        .when('/dashboard/',
        {
            controller: 'DashboardController',
            templateUrl: 'ng-templates/dashboard.html'

        })

        .when('/signup/',
        {
            controller: 'SignupController',
            templateUrl: 'ng-templates/signup.html'

        })

        .when('/login/',
        {
            controller: 'LoginController',
            templateUrl: 'ng-templates/login.html'

        })

        .when('/newQuiz/',
        {
            controller: 'NewQuizController',
            templateUrl: 'ng-templates/newQuiz.html'

        })
		.when('/templateQuiz/',
        {
            controller: 'TemplateQuizController',
            templateUrl: 'ng-templates/templateQuiz.html'

        })
		.when('/qapageQuiz/',
        {
            controller: 'QAPageQuizController',
            templateUrl: 'ng-templates/qapageQuiz.html'

        })
        .when('/thankuQuiz/',
        {
            controller: 'thankuQuizController',
            templateUrl: 'ng-templates/thankuQuizController.html'

        })
        .when('/finalQuiz/',
        {
            controller: 'finalQuizController',
            templateUrl: 'ng-templates/finalQuizController.html'

        })

        // if non of the above routes
        // are matched we are setting router
        // to redirect to the RootController
        .otherwise({ redirectTo: '/'});
});


function hideLoading() {
    document.getElementById("loading_mask").style.width = "0%";
}


ngApp.controller('LoadingController', function($scope) {
    //highlight right tab
    $scope.$on('$viewContentLoaded', function() {
        hideLoading();
    });
});


ngApp.factory('Utils',function($scope, $timeout, $location){

    var UtilService = {};
    UtilService.showLoading = function() {
        document.getElementById("loading_mask").style.width = "100%";
    }
    UtilService.removeLoading = function() {
        document.getElementById("loading_mask").style.width = "0%";
    }
    return UtilService;
});


//init when document ready
angular.element(document).ready(function() {
    angular.bootstrap(document, ['myApp']);
});
