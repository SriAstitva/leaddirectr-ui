ngApp.controller('TemplateQuizController', ['$scope', '$location',
  function ($scope, $location) {
  	$scope.chooseTemplate = [{"TmpName":"Select a Template"},{"TmpName":"Tmp1.html"},{"TmpName":"Tmp2.html"},{"TmpName":"Tmp3.html"},{"TmpName":"Tmp4.html"}];
	$scope.formTN = {templateChangedValue :$scope.chooseTemplate[0].TmpName};
	
  	$scope.chooseFont = [{"FontName":"Select Font"},{"FontName":"Arial"},{"FontName":"sans-serif"},{"FontName":"serif"},{"FontName":"monospace"}];
	$scope.formFN = {fontChangedValue :$scope.chooseFont[0].FontName};
	
	$scope.chooseColor = [{"ColorName":"White"},{"ColorName":"Green"},{"ColorName":"Red"},{"ColorName":"Blue"}];	
	$scope.formCN = {colorChangedValue :$scope.chooseColor[0].ColorName};
	
	$scope.textStyles = {
		'font-family': $scope.formFN.fontChangedValue,
		'color': $scope.formCN.colorChangedValue
	};
	$scope.fontApply = function(){
		$scope.textStyles['font-family'] = $scope.formFN.fontChangedValue;
	};
	$scope.fontColor = function(){
		$scope.textStyles['color'] = $scope.formCN.colorChangedValue;
	};
	$scope.nextStep = function(){
		$location.path( "/qapageQuiz" );
	}	
  	
	$scope.templateTitle = 'New Smart Phone';
	$scope.templateComment = 'Be the first to know about different promos at TemplateMonster website.';
	$scope.actionText = '';
	$scope.image = null;
  $scope.imageFileName = '';
  
  $scope.uploadme = {};
  $scope.uploadme.src = 'img/quizstep2.png';

  }
]);

ngApp.directive('fileDropzone', function() {
  return {
    restrict: 'A',
    scope: {
      file: '=',
      fileName: '='
    },
    link: function(scope, element, attrs) {
      var checkSize,
          isTypeValid,
          processDragOverOrEnter,
          validMimeTypes;
      
      processDragOverOrEnter = function (event) {
        if (event != null) {
          event.preventDefault();
        }
        event.dataTransfer.effectAllowed = 'copy';
        return false;
      };
      
      validMimeTypes = attrs.fileDropzone;
      
      checkSize = function(size) {
        var _ref;
        if (((_ref = attrs.maxFileSize) === (void 0) || _ref === '') || (size / 1024) / 1024 < attrs.maxFileSize) {
          return true;
        } else {
          alert("File must be smaller than " + attrs.maxFileSize + " MB");
          return false;
        }
      };

      isTypeValid = function(type) {
        if ((validMimeTypes === (void 0) || validMimeTypes === '') || validMimeTypes.indexOf(type) > -1) {
          return true;
        } else {
          alert("Invalid file type.  File must be one of following types " + validMimeTypes);
          return false;
        }
      };
      
      element.bind('dragover', processDragOverOrEnter);
      element.bind('dragenter', processDragOverOrEnter);

      return element.bind('drop', function(event) {
        var file, name, reader, size, type;
        if (event != null) {
          event.preventDefault();
        }
        reader = new FileReader();
        reader.onload = function(evt) {
          if (checkSize(size) && isTypeValid(type)) {
            return scope.$apply(function() {
              scope.file = evt.target.result;
              if (angular.isString(scope.fileName)) {
                return scope.fileName = name;
              }
            });
          }
        };
        file = event.dataTransfer.files[0];
        name = file.name;
        type = file.type;
        size = file.size;
        reader.readAsDataURL(file);
        return false;
      });
    }
  };
})


.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);