ngApp.controller('DashboardController', ['$scope',
  function ($scope) {
    $scope.dashboardData = [{
      "record_id": 1,
      "date": '12/02/2017',
      "quizName": 'Lorem Ipsum, Proin gravida nibh vel velit auctor aliquet',
      "quizLink": 'www.helth.com',
      "views": 52,
      "leads": 23,
      "shares": 65,
      "CTAClicks": 10,
      "conversionRate": 10.00,
      "status": "Archived"
    },{
      "record_id": 2,
      "date": '12/02/2017',
      "quizName": 'Velit auctor aliquet, Aenean sollicitudin',
      "quizLink": 'www.helth.com',
      "views": 52,
      "leads": 23,
      "shares": 65,
      "CTAClicks": 10,
      "conversionRate": 10.00,
      "status": "Drafted"
    },{
      "record_id": 3,
      "date": '12/02/2017',
      "quizName": 'Velit auctor aliquet, Aenean sollicitudin',
      "quizLink": 'www.helth.com',
      "views": 52,
      "leads": 23,
      "shares": 65,
      "CTAClicks": 10,
      "conversionRate": 10.00,
      "status": "Published"
    }];
    $scope.search = function(){
      if(!$scope.statusType && !$scope.quizType && !$scope.searchText){

      } else if(!$scope.statusType && !$scope.quizType && $scope.searchText){

      } else if(!$scope.statusType && $scope.quizType && !$scope.searchText){

      } else if($scope.statusType && !$scope.quizType && !$scope.searchText){

      } else if(!$scope.statusType && $scope.quizType && $scope.searchText){

      } else if($scope.statusType && !$scope.quizType && $scope.searchText){

      } else if($scope.statusType && $scope.quizType && !$scope.searchText){

      } else if($scope.statusType && $scope.quizType && $scope.searchText){

      }
    };
    $scope.sortColumn = 'quizName';
    $scope.reverseSort = false;
  $scope.sortData = function(column){
    $scope.reverseSort = ( $scope.sortColumn == column) ? !$scope.reverseSort : false ;
    $scope.sortColumn = column;
  };
  $scope.getSortClass = function(column){
    if($scope.sortColumn == column){
      return $scope.reverseSort ? 'fa fa-caret-down' : 'fa fa-caret-up' ;
    }
    return '';
  };
  }
]);