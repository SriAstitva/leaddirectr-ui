ngApp.controller('SignupController', ['$scope',
  function ($scope) {
  	$scope.submitForm = function(isValid) {
  		$scope.errorMsg = '';
	    if (isValid) {
	    	if($scope.password != $scope.confirmPassword){
	    		$scope.errorMsg = "Password and Confirm Password are not same.";
	    		return false;
	    	}
	    } else {
	    	$scope.errorMsg = "Please check all the fields.";
	    	return false;
	    }
  	};
  }
]);