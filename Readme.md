# Base Angular Project for Web Application #


## How to use: ##
1. copy/fork the directory
1. checkout the fork
1. run "npm install" to install gulp and dev depenedencies
1. npm install http-server -g (or any other http server)
1. run "http-server ." (or start the server you downloaded in 3)

## This sample has: ##
1. angular 1.5
1. angular-ui support (bootstrap for angular)
1. uses templates for controllers
1. router in main.js
1. gulp to watch and build

## Important files: ##
1. index.html -> inits and loads everything - css, js, etc
1. main.js -> main js file to init angular application
1. bundle.js -> built after minification

## Workflow: ##
1. In one session start the web server (run "http-server .")
1. In another run "gulp watch"
1. Now all changes in "js/ng/" folder will be picked up and compiled in bundle